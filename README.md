---
title: README
date: "{{DATE}}"
---
## {{site.name| upcase}} *holo*GIT Repositories


Welcome to the Krysthal Intelligence Repositories (a.k.a [KIR])

<form action=https://framagit.org/search method=GET>
<input name=search value="holo*" placeholder="your holoRepository search">
</form>

we have 3 GITs repository sets:

* [holoKIN](https://gitlab.com/kin4) ([search](https://gitlab.com/search?search=holo&group_id=7816194))
* [holoTools](https://framagit.org/holotools4)
* [holoTeam](https://framagit.org/holoTeam)

api: {{site.apiid}}


[KIN]: {{site.search}}=%23KIN


## Overview of *holo*GIT Repositories

* HoloSphere :
  - https://gitlab.com/holosphere 
  - gitlab: [search](https://gitlab.com/search?search=holo&group_id=7816696)
  - <https://framagit.org/holosphere>

* MLP : https://gitlab.com/kin4/MLP [search](https://gitlab.com/search?search=holo&group_id=7816696)
  - holoWiki : https://kin4.gitlab.io/holowiki
  - holoBook : https://holosphere4.gitlab.io/holobook
  - holoBin : https://framagit.org/search?search=holoBin

* KIN : https://gitlab.io/kin4/
  - holoBook : https://kin4.gitlab.io/holobook
  - profile template : https://kin4.gitlab.io/holoprofile
  - holoLabs : https://gitlab.com/kin4/hololabs
    
* WIP :
    - gitlab: [search](https://gitlab.com/search?search=holo&group_id=7816696)

### framaGit Repository

* holoTeam : https://framagit.org/holoTeam
  - holoNotes : https://framagit.org/holoteam/holonotes


### full index

* <https://ipfs.io/ipfs/{{site.data.repo.qmrepo}}/> (as of {{site.data.repo.date}})
* <https://ipfs.io/ipns/QmauatkWxdjZQSdxGoafb4yBKpZJNhpj7N3vh7rtof5DnS/
* <https://🌕.gitlab.io/overview>

[![status](https://www.repostatus.org/badges/latest/moved.svg)](https://gitlab.com/xn--1g8h/overview)
